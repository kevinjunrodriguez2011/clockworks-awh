﻿using Clockwork.Models.API;
using Microsoft.EntityFrameworkCore;

namespace Clockwork.DatabaseCore
{
    public class ClockworkContext : DbContext
    {
        public ClockworkContext(DbContextOptions options) 
            : base(options) {
        }
        public DbSet<CurrentTimeQuery> CurrentTimeQueries { get; set; }
    }
}
