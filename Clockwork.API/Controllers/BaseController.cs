﻿using System;
using Clockwork.DatabaseCore;
using Microsoft.AspNetCore.Mvc;

namespace Clockwork.API.Controllers
{
    public class BaseController : Controller
    {
        protected ClockworkContext ClockWorkDb { get; }

        public BaseController(ClockworkContext _clockworkContext)
        {
            ClockWorkDb = _clockworkContext;
        }  
        public void SaveChanges()
        {
            try
            {
                ClockWorkDb.SaveChanges();
            }
            catch (Exception ex)
            {
                //add logger
                throw;
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ClockWorkDb.Dispose();
                base.Dispose(disposing);
            }
        }
    }
}