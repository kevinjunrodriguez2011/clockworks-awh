﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clockwork.Models.API;
using Clockwork.DatabaseCore;

namespace Clockwork.API.Controllers
{
    [Route("api/[controller]")]
    public class CurrentTimeController : BaseController
    {
        public CurrentTimeController(ClockworkContext clockworkContext) 
            : base(clockworkContext) {
        }
        // GET api/currenttime
        [HttpGet]
        public IActionResult Get()
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();

            var returnVal = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime,
                TimeZone = "SG"
            };
            ClockWorkDb.CurrentTimeQueries.Add(returnVal);
            var count = ClockWorkDb.SaveChanges();

            Console.WriteLine("{0} records saved to database", count);
            Console.WriteLine();
            foreach (var CurrentTimeQuery in ClockWorkDb.CurrentTimeQueries)
            {
                Console.WriteLine(" - {0}", CurrentTimeQuery.UTCTime);
            }

            return Ok(returnVal);
        }
        public IActionResult GetTime()
        {
            IReadOnlyCollection<CurrentTimeQuery> currentTimeQueries = ClockWorkDb.CurrentTimeQueries.ToList();

            if (currentTimeQueries == null)
            {
                return Json(new { hasError = true, Message = "Time list is empty" });
            }
            return Json(new { TimeList = currentTimeQueries });

        }
        [HttpPost]
        public IActionResult SaveTime()
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            string ipAddr = this.HttpContext.Connection.RemoteIpAddress.ToString();

            ClockWorkDb.CurrentTimeQueries.Add(new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ipAddr,
                Time = serverTime
            });

            SaveChanges();

            return Ok();
        }
        [HttpPut]
        public IActionResult UpdateTime(CurrentTimeQuery currentTimeQuery)
        {
            CurrentTimeQuery currentTime = ClockWorkDb.CurrentTimeQueries.Find(currentTimeQuery.CurrentTimeQueryId);

            currentTime.Time = currentTimeQuery.Time;
            currentTime.UTCTime = currentTimeQuery.UTCTime;

            if (currentTime == null)
            {
                return Json(new { hasError = true, Message = "Error updating time" });
            }
            ClockWorkDb.Entry(currentTime).State = EntityState.Modified;

            SaveChanges();

            return Ok();
        }
    }
}
