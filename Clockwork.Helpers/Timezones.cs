﻿using System;
using System.Collections.Generic;

namespace Clockwork.Helpers
{
    public class Timezones
    {
        public IReadOnlyCollection<TimeZoneInfo> GetTimeZones()
        {
            return TimeZoneInfo.GetSystemTimeZones();
        }
        public DateTime GetDateTimeFromTimeZone(TimeZoneInfo timeZoneInfo)
        {
            DateTime currentDateTime = DateTime.UtcNow;
            return TimeZoneInfo.ConvertTimeFromUtc(currentDateTime, TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo.Id));
        }
    }
}
